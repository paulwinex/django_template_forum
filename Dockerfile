FROM python:3.6
RUN apt-get update &&\
    apt-get install -yqq mc curl &&\
    mkdir -p /data
COPY ./src /data/forum/
WORKDIR /data/forum/
RUN pip install -r /data/forum/requirements.txt
EXPOSE 8080
ENTRYPOINT ??????
